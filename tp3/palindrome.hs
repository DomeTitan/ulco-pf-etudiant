main :: IO()
main = do
    line <- getLine
    print(reverse line == line)