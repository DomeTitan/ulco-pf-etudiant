main :: IO()
main = do
    print(fuzzyLength [])
    print(fuzzyLength [1])
    print(fuzzyLength [1, 2])
    print(fuzzyLength [1..4])

fuzzyLength :: [a] -> String
fuzzyLength [] = "empty"
fuzzyLength [x] = "one"
fuzzyLength [x, y] = "two"
fuzzyLength _ = "many"

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x