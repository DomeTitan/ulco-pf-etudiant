
main :: IO ()
main = do
    print (myFst ("foo", 1))
    print (mySnd ("foo", 1))

myFst :: (a, b) -> a
myFst (x, _) = x

mySnd :: (a, b) -> b
mySnd (_, y) = y