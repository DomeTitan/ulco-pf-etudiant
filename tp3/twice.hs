main :: IO()
main = do
    print (twice[1, 2])
    print (twice "to")
    print (sym[1, 2])
    print (sym "to")

twice :: [a] -> [a]
twice l = l ++ l

sym :: [a] -> [a]
sym l = l ++ (reverse l)