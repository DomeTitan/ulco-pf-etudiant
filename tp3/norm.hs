main :: IO()
main = do
    print (dot [3, 4] [1, 2])
    print (dot [3, 4] [3, 4])

dot :: [Double] -> [Double] -> Double
dot u v = sum (zipWith (*) u v)