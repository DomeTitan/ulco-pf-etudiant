main :: IO ()
main = do
    print(fibo 10)


fibo :: Int -> Int
fibo x = case x of
        0 -> 0
        1 -> 1
        _ -> fibo (x - 1) + fibo (x - 2)