import Text.Read

main :: IO()
main = do
    line <- getLine
    let nombre = readMaybe line :: Maybe Int
    print(nombre)
