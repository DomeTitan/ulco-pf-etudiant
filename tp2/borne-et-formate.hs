main :: IO()
main = do
    putStr("Entrez un nombre : ")
    line <- getLine
    let nombre = read line
    putStrLn(borneEtFormate1 nombre)
    putStrLn(borneEtFormate2 nombre)

borneEtFormate1 :: Double -> String
borneEtFormate1 x = 
    show x ++ " -> " ++ show x2
    where x2 = 
            if x < 0
            then 0
            else if x > 1
                then 1
                else x

borneEtFormate2 :: Double -> String
borneEtFormate2 x =
    show x ++ " -> " ++ show x2
    where x2 | x < 0 = 0
             | x > 1 = 1
             | otherwise = 0