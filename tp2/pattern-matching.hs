main :: IO()
main = do
    print(fibo 9)

fibo :: Int -> Int
fibo 0 = 0
fibo 1 = 1
fibo x = fibo(x - 1) + fibo(x - 2)