main :: IO ()
main = do
    putStr("Entrez un nombre : ")
    line <- getLine
    let nombre = read line
    putStrLn(formaterParite nombre)
    putStrLn(formaterSigne nombre)
    putStrLn(formaterParite2 nombre)
    putStrLn(formaterSigne2 nombre)

formaterParite :: Int -> String
formaterParite x = do
    if even x
        then "pair" 
        else "impaire"

formaterSigne :: Int -> String
formaterSigne x = do
    if x == 0
        then "nul"
        else if x < 0
                then "negatif"
                else "positif"

formaterParite2 :: Int -> String
formaterParite2 x 
    | even x = "pair"
    | otherwise = "impaire"

formaterSigne2 :: Int -> String
formaterSigne2 x
    | x == 0 = "nul"
    | x < 0 = "négatif"
    | otherwise = "positif"