import Text.Read

main :: IO()
main = do
    print(safeSqrt 16)
    print(safeSqrt 0)

safeSqrt :: Double -> Maybe Double
safeSqrt x =
    if x > 0
    then Just (sqrt x)
    else Nothing