import System.Random

main :: IO ()
main = do
    target <- randomRIO(1, 100)
    run target

run :: Int -> IO()
run target = do
    putStr("Type a number : ")
    line <- getLine
    let nombre = read line
    if nombre == target
        then putStr("You win!")
        else do
            putStrLn(if nombre < target then "Too small" else "Too big")
            run target