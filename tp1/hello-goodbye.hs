main :: IO ()
main = do
    putStr("What's your name : ")
    nom <- getLine
    if nom == ""
        then putStr("Goodbye!")
        else do
            putStrLn("Wello " ++ nom ++ "!")
            main