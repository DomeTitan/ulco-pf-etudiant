main :: IO()
main = do
    print (fact 2)
    print (fact 5)

fact :: Int -> Int
fact 1 = 1
fact x = x * fact (x - 1)