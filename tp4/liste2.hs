main :: IO()
main = do
    --print(mulListe 3 [3, 4, 1])
    print(selectListe (2, 3) [3, 4, 1])

mulListe :: Int -> [Int] -> [Int]
mulListe _ [] = []
mulListe n (x:xs) = n*x : mulListe n xs

selectListe :: (Int, Int) -> [Int] -> [Int]
selectListe tuple liste = aux tuple liste []
    where
        aux _ [] listeFinale = listeFinale
        aux (a,b) (l:ls) listeFinale = do
            if a <= l && l <= b
                then
                    aux (a,b) ls (listeFinale ++ [l])
                else
                    aux (a,b) ls listeFinale

sumListe :: [Int] -> Int
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs