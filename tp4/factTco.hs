main :: IO()
main = do
    print (factTco 2)
    print (factTco 5)

factTco :: Int -> Int
factTco x = aux x 1
    where aux 1 acc = acc
          aux n acc = aux (n - 1) (acc * n)
