import Data.Char

main :: IO()
main = do
    print (myLength "foobar")
    print (toUpperString "foo bar")
    print (onlyLetters "t1oto")

myLength :: [a] -> Int
myLength l = aux l 0
    where 
        aux [] n = n
        aux (_:xs) n = aux xs (n + 1)

toUpperString :: String -> String
toUpperString s = aux s ""
    where 
        aux "" string = string
        aux (c:cs) string = aux cs (string ++ [toUpper c])

onlyLetters :: String -> String
onlyLetters s = aux s ""
    where
        aux "" string = string
        aux (c:cs) string = do
            if isLetter c
                then aux cs (string ++ [c])
                else aux cs string