main :: IO()
main = do
    result <- loopEcho 3
    putStrLn result

loopEcho :: Int -> IO String
loopEcho 0 = return "Loop termined"
loopEcho x = do
    putStr "> "
    line <- getLine
    if line /= ""
        then do
            putStrLn line
            loopEcho (x - 1)
        else return "empty line"