main :: IO()
main = do
    print (map (\x -> x*2) [1..4])
    print (map (\x -> x/2) [1..4])
    print (map (\(c, i) -> c : '-' : show i) (zip ['a' .. 'z'] [1..]))
    print (zipWith (\ c i -> c : '-' : show i) ['a' .. 'z'] [1..])