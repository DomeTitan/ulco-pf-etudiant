main :: IO()
main = do
    print (myTake2 [1..4 :: Int])
    print (myGet2 [1..4 :: Int])  
    print (myMapPar2 [1..4])
    print (myMap2 [1..4])

myTake2 :: [a] -> [a]
myTake2 = take 2

myGet2 :: [a] -> a
myGet2 = (flip (!!)) 2

myMapPar2 :: [Int] -> [Int]
myMapPar2 = map (*2)

myMap2 :: [Int] -> [Int]
myMap2 = map (`div` 2)