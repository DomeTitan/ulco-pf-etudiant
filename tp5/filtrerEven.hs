main :: IO()
main = do
    print (filtrerEven1 [1..4])
    print (filtrerEven2 [1..4])

filtrerEven1 :: [Int] -> [Int]
filtrerEven1 [] = []
filtrerEven1 (x:xs) = do
    if even x
        then x : filtrerEven1 xs
        else filtrerEven1 xs

filtrerEven2 :: [Int] -> [Int]
filtrerEven2 l = filter even l

myFiltrer :: (a -> Bool) -> [a] -> [a]
myFiltrer _ [] = []
myFiltrer p (x:xs) = do
    if p x
        then x : myFiltrer p xs
        else myFiltrer p xs