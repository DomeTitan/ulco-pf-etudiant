main :: IO()
main = do
    print (mapDoubler1 [1..4])
    print (mapDoubler2 [1..4])
    print (myMap (*2) [1..4 :: Int])

mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = x * 2 : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 l = map (*2) l

myMap :: (a -> a) -> [a] -> [a]
myMap _ [] = []
myMap f (x:xs) = (f x) : myMap f xs