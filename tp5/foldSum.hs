main ::IO ()
main = do
    print (foldSum1 [1..4])
    print (foldSum2 [1..4])
    print (myFoldl (+) 0 [1..4])

foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 l = foldr (+) 0 l

myFoldl :: (b -> a -> b) -> b -> [a] -> b
myFoldl _ acc [] = acc
myFoldl f acc (x:xs) = myFoldl f (f acc x) xs