main :: IO()
main = do
    print (doubler [1..4])
    print (pairs [1..4])
    print (multiples 35)
    print (tripletsPyth 13)

doubler :: [Int] -> [Int]
doubler l = [x * 2 | x <- l]

pairs :: [Int] -> [Int]
pairs l = [x | x <-l, even x]

multiples :: Int -> [Int]
multiples n = [ x | x <- [1..n], n `mod` x == 0 ]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [ (x, y, z) | x <- [1..n], y <- [x..n], z <- [1..n], x^2 + y^2 == z^2 ]